/**
 * Копирует поля объекта B в объект-шаблон А
 * @param   {object} a Объект-шаблон
 * @param   {object} b Объект значения
 * @returns {object} Объект объединения A с B
 */
function assignObj(a, b) {
	var c = {};
	for (var key in a)
		c[key] = (b[key]) ? b[key] : a[key];
	return c;
}
/**
 * Класс шага
 * @classdesc Описывает шаг и его методы, такие как активация, следующий, предыдущий и передача данных
 * @param {object} options Объект параметров, состоящий из следующих значений
 *                         active: является ли этот шаг по умолчанию активным? --> true/false  | Default: false
 *                         disabled: является ли этот шаг по умолчанию отключенным? true/false --> | Default: false
 *                         index: номер шага в DOM-Element #stepper > .step -> number  | Default: 0
 *                         onload: функция для вызова при отображении шага (указывается без параметров) --> function  | Default: function () {}
 */
function Step(options) {
	this.options = {
		active: false,
		disabled: false,
		index: 0,
		onload: function () {}
	}
	this.options = assignObj(this.options, options);
}
Step.prototype = {
	/**
	 * Активирует шаг, подкручивает счетчик, проверяет кнопки
	 */
	activate: function () {
		if (!this.options.disabled) {
			this.options.active = true;
			var step_counter = document.querySelector("#step-current");
			var num = 0;
			for (var i = 0; i < steps_arr.length; i++)
				if (steps_arr[i].options.active)
					step_counter.innerHTML = i+1;
				else if (!steps_arr[i].options.disabled) num++;
			step_counter.setAttribute("data-count", num+1);
			document.querySelector("#stepper > .step:nth-child(" + (this.options.index+1) + ")").setAttribute("current", "");
			var trigg = false;
			for (var i = 0; i < steps_arr.length; i++)
				if (steps_arr[i].options.index > this.options.index)
					if (!steps_arr[i].options.disabled)
						trigg = true;
			if (!trigg)
				document.querySelector("#nextBtn").setAttribute("nobtn", "");
			else
				document.querySelector("#nextBtn").removeAttribute("nobtn");
			var trigg = false;
			for (var i = 0; i < steps_arr.length; i++)
				if (steps_arr[i].options.index < this.options.index)
					if (!steps_arr[i].options.disabled)
						trigg = true;
			if (!trigg)
				document.querySelector("#backBtn").setAttribute("nobtn", "");
			else
				document.querySelector("#backBtn").removeAttribute("nobtn");
			this.options.onload();
		}
	},
	/**
	 * Откатывается на шаг назад
	 */
	back: function () {
		var current_step = document.querySelector("#step-current").innerHTML - "";
		current_step--;
		if (steps_arr[current_step].options.index > 0) {
			document.querySelector(".step[current]").removeAttribute("current");
			steps_arr[current_step].options.active = false;
			var to_activate = 0;
			for (var i = current_step; i > -1; i--)
				if (!steps_arr[i].options.disabled)
					if (steps_arr[i].options.index < steps_arr[current_step].options.index) {
						to_activate = i;
						break;
					}
			steps_arr[to_activate].activate();
		}
	},
	/**
	 * Переходит на следующий шаг
	 */
	next: function () {
		document.querySelector(".step[current]").removeAttribute("current");
		var current_step = document.querySelector("#step-current").innerHTML - "";
		current_step--;
		steps_arr[current_step].options.active = false;
		var to_activate = 0;
		for (var i = 0; i < steps_arr.length; i++)
			if (!steps_arr[i].options.disabled)
				if (steps_arr[i].options.index > steps_arr[current_step].options.index) {
					to_activate = i;
					break;
				}
		steps_arr[to_activate].activate();
	},
	/**
	 * Отправляет данные текущей формы и при удаче переходит на следующий шаг
	 */
	sendData: function () {
		var str = window.location.search;
		var _this = this;
		$.ajax({
			type: 'POST',
			url: "/set_data_order",
			data: { order: str.substr(str.indexOf("edit=")+5, str.length), data: $(".sten[current] form").serialize() },
			success: function (c) {
				_this.next();
			},
			async: true
		});
	}
}
document.addEventListener("DOMContentLoaded", function () {
	var act = -1;
	for (var i = 0; i < steps_arr.length; i++) {
		steps_arr[i] = new Step(steps_arr[i]);
		if (steps_arr[i].options.active)
			act = i;
	}
	if (act != -1)
		steps_arr[i].activate();
	else {
		for (var i = 0; i < steps_arr.length; i++) {
			if (!steps_arr[i].options.disabled) {
				steps_arr[i].activate();
				break;
			}
		}
	}
});
